package Client;

import com.soap.webservice.TestMartCatalog;
import com.soap.webservice.TestMartCatalogService;

public class Client {
	public static void main(String[] args) {
		TestMartCatalogService catalogService = new TestMartCatalogService();

		TestMartCatalog testMartCatalog = catalogService
				.getTestMartCatalogPort();

		System.out.println(testMartCatalog.fetchCategories("books"));
		
		testMartCatalog.addProduct("books", "Nishant ebooks", 1);
		
		System.out.println(testMartCatalog.fetchCategories("books"));
	}
}

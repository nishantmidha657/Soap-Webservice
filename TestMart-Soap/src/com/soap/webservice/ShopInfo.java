package com.soap.webservice;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

/**
 * 
 * @author Nishant
 *
 *Describing schemaTypes and binding info of wsdl file
 *By default type information is getting generate in different xsd 
 *To OverRide use soap binding(Documnet is by Default) use Stype.rpc TO OVERRIDES
 *Binding tag now would have stype RPC which will not generate separate schema for types rather would create inline types
 */

/**
 * 
 * Soap Binding can be RPC or DOCUMENT
 *
 * DOCUMNET :- Create separate schema for type and message element refer to
 * elemnet of from new schema
 *
 * RPC:- No separate schema documnet will be created . Type information would be
 * available in same documnet and message element have inline paramater
 * information
 * 
 * @WebParam and @WebResult anno is used to set proper required name to be used for paramaters and returntype name 
 * by default it uses arg0 ,agr1 for input nad result for output 
 * 
 * There are advantages of using DOCUMNET Type as it helps us to validate the request and response against the schema(minarg attribute) but we lose this in RPC
 */

@WebService
@SOAPBinding(style = Style.RPC)
public class ShopInfo {

	@WebMethod
	@WebResult(partName = "lookupOutput")
	public String getShopInfo(@WebParam(partName="lookUpInput")String property, int a) throws InvaliudInputException {
		String response = "Invalid property";
		if ("shopName".equals(property)) {
			response = "Test Mart";
		} else if ("since".equals(property)) {
			response = "Since 2012";
		} else{
			throw new InvaliudInputException("Invalid Input", property + "is not a valid input");
		}

		return response;

	}
}
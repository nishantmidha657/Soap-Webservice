package com.soap.webservice;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import com.soap.webservice.business.ProductServiceImpl;
import com.soap.webservice.model.Product;
/**
 * 
 * @author Nishant
 *
 *Earlier specification for defining service in soap was to define a SEI(service endpoint interface ) which would contain all the service methods required to be exposed and all the annotations would be in interface only 
 *as shown in ProductcatalogInterface
 */
//target name space are equivalent to packages in java world
//we can override targetNameSpace
@WebService(endpointInterface = "com.soap.webservice.ProductCatalogInterface" , name = "TestMartCatalog", portName = "TestMartCatalogPort", serviceName = "TestMartCatalogService")
//@WebService(name = "TestMartCatalog", portName = "TestMartCatalogPort", serviceName = "TestMartCatalogService" , targetNamespace = "www.testMart.com")
public class ProductCatalog implements ProductCatalogInterface {
	ProductServiceImpl productServiceImpl;

	public ProductCatalog() {
		productServiceImpl = new ProductServiceImpl();
	}

	/* (non-Javadoc)
	 * @see com.soap.webservice.ProductCatalogInterface#getProducts(java.lang.String)
	 */
	@Override
	//@WebMethod(action = "fetch_categories" , operationName = "FetchCategories")
	public List<String> getProducts(String category) {
		return productServiceImpl.getProducts(category);
	}

	/* (non-Javadoc)
	 * @see com.soap.webservice.ProductCatalogInterface#addProduct(java.lang.String, java.lang.String, int)
	 */
	@Override
//	@WebMethod
	public boolean addProduct(String category, String product ,int value) {
		return productServiceImpl.addProducts(category, product);
	}

	/* (non-Javadoc)
	 * @see com.soap.webservice.ProductCatalogInterface#getProductCatalog()
	 */
	@Override
	//@WebMethod
	public List<String> getProductCatalog() {
		return productServiceImpl.getProductCategories();
	}

	/* (non-Javadoc)
	 * @see com.soap.webservice.ProductCatalogInterface#getProductsV2(java.lang.String)
	 */
	@Override
	//@WebMethod
	public List<Product> getProductsV2(String category) {
		return productServiceImpl.getProductsV2(category);
	}
	
}

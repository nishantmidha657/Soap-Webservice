package com.soap.webservice;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.soap.webservice.model.Product;
@WebService(name = "TestMartCatalog", portName = "TestMartCatalogPort", serviceName = "TestMartCatalogService" , targetNamespace = "www.testMart.com")
public interface ProductCatalogInterface {
	@WebMethod(action = "fetch_categories" , operationName = "FetchCategories")
	public abstract List<String> getProducts(String category);
	@WebMethod
	public abstract boolean addProduct(String category, String product,
			int value);
	@WebMethod
	public abstract List<String> getProductCatalog();
	@WebMethod
	@WebResult(name = "Product")
	public abstract List<Product> getProductsV2(String category);

}
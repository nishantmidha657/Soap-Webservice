package com.soap.webservice.business;

import java.util.ArrayList;
import java.util.List;

import com.soap.webservice.model.Product;

public class ProductServiceImpl {

	List<String> bookList = new ArrayList<>();
	List<String> musicList = new ArrayList<>();
	List<String> movieList = new ArrayList<>();

	public ProductServiceImpl() {
		bookList.add("Inferno");
		bookList.add("JoyLand");
		bookList.add("The Game of Thrones");

		musicList.add("Random Accee memories");
		musicList.add("Night Vision");
		musicList.add("JukeBox");

		movieList.add("Star Trek");
		movieList.add("Despicable Me");
		movieList.add("Beauty & Beasts");

	}

	public List<String> getProductCategories() {
		List<String> categories = new ArrayList<>();

		categories.add("Movies");
		categories.add("Books");
		categories.add("Music");

		return categories;
	}

	public List<String> getProducts(String category) {
		switch (category.toString()) {
		case "books":
			return bookList;
		case "music":
			return musicList;
		case "movies":
			return movieList;
		}
		return null;
	}

	public boolean addProducts(String category, String product) {
		switch (category.toString()) {
		case "books":
			bookList.add(product);
			break;
		case "music":
			musicList.add(product);
			break;
		case "movies":
			movieList.add(product);
			break;
		default:
			return false;
		}
		return true;
	}

	public List<Product> getProductsV2(String category) {
		List<Product> products = new ArrayList<>();
		products.add(new Product("Java Book", "1234", 9999));
		products.add(new Product("Another Java Book", "123", 99));

		return products;
	}
}
